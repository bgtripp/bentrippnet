/* global PIXI */
export default class {
  constructor() {
    this.app = new PIXI.Application(window.innerWidth, window.innerHeight, {backgroundColor : 0x99cccc});
    document.getElementById('canv').appendChild(this.app.view);

    this.first = true

    // create a texture from an image path
    this.poly1 = new PIXI.Sprite.fromImage('/poly1.png');
    this.thing2 = new PIXI.Sprite.fromImage('/thing2.png');
    this.ellipse = new PIXI.Sprite.fromImage('/ellipse.png');
    this.ellipse2 = new PIXI.Sprite.fromImage('/ellipse2.png');

    this.thing2.x = this.app.screen.width/2;
    this.thing2.y = this.app.screen.height/2;
    this.thing2.scale.set(0.5);
    this.thing2.anchor.set(0.5);

    this.poly1.x = this.app.screen.width/2;
    this.poly1.y = this.app.screen.height/2;
    this.poly1.scale.set(0.5);
    this.poly1.anchor.set(0.5);

    this.ellipse.x = this.app.screen.width/2;
    this.ellipse.y = this.app.screen.height/2;
    this.ellipse.scale.set(0.5);
    this.ellipse.anchor.set(1.4);

    this.ellipse2.x = this.app.screen.width/2;
    this.ellipse2.y = this.app.screen.height/2;
    this.ellipse2.scale.set(0.5);
    this.ellipse2.anchor.set(2);

  }

  rotate () {
    let thing = this.thing2
    let poly = this.poly1
    let ellipse = this.ellipse
    let ellipse2 = this.ellipse2
    let h = this.h
    let app = this.app

    this.app.ticker.add(function(delta) {
      if (ellipse2.anchor.x >= 1 || ellipse2.anchor.y >= 1) {
        ellipse2.anchor.set(ellipse2.anchor.x-0.02, ellipse.anchor.y-0.02)
      } else {
        ellipse2.anchor.set(ellipse2.anchor.x+0.02, ellipse.anchor.y+0.02)
      }
      if (ellipse.anchor.x <= 2 || ellipse.anchor.y <= 2) {
        ellipse.anchor.set(ellipse.anchor.x+0.02, ellipse.anchor.y+0.02)
      } else {
        ellipse.anchor.set(ellipse.anchor.x-0.02, ellipse.anchor.y-0.02)
      }
      console.log(ellipse2.anchor)
      thing.rotation -= 0.05 * delta;
      poly.rotation += 0.05 * delta;
      ellipse.rotation -= 0.07  * delta;
      ellipse2.rotation += 0.045 * delta;
    });
  }

  mobile () {
    this.poly1.scale.set(0.3);
    this.thing2.scale.set(0.3);
    this.ellipse.scale.set(0.3);
    this.ellipse2.scale.set(0.3);
  }

  activate () {
    this.app.stage.addChild(this.thing2);
    this.app.stage.addChild(this.poly1);
    this.app.stage.addChild(this.ellipse);
    this.app.stage.addChild(this.ellipse2);

    this.ellipse2.anchor.set(2);

    if (this.first) {
      this.rotate();
      this.first = false;
    }

  }

  deactivate () {
    this.app.stage.removeChild(this.thing2);
    this.app.stage.removeChild(this.poly1);
    this.app.stage.removeChild(this.ellipse);
    this.app.stage.removeChild(this.ellipse2);
  }


}
