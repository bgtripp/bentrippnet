const consonants = [
  "b",
  "c",
  "d",
  "f",
  "g",
  "h",
  "j",
  "k",
  "l",
  "m",
  "n",
  "p",
  "r",
  "s",
  "t",
  "v",
  "w",
  "x",
  "y",
  "z",
  "y"
]

const vowels = [
  "a",
  "e",
  "i",
  "o",
  "u"
]

var startCons = [
  "br",
  "dr",
  "gr",
  "kr",
  "pr",
  "tr",
  "vr",
  "wr",

  "ch",
  "gh",
  "ph",
  "sh",
  "wh",

  "sl",
  "fl",
  "gl",
  "pl",
  "bl",
  "cl",

  "st",

  "qu"
]

var endCons = [
  "lk",
  "ck",
  "mp",
  "ss",
  "ll",
  "rn"
]

var midVowels = [
  "ea",
  "ee",
  "ei",
  "ou",
  "ay",
  "au"
]

var midCons = [
  "ll",
  "rr",
  "ss",
  "mm"
]

startCons = consonants.concat(startCons);
endCons = consonants.concat(endCons);
midVowels = vowels.concat(midVowels);
midCons = consonants.concat(midCons);

export { consonants, vowels, startCons, endCons, midVowels, midCons };
