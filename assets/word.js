import { consonants, vowels, startCons, endCons, midVowels, midCons } from '~/assets/sounds.js'

export default function (max) {
  let constLast = true;
  let start = '';
  let mid = '';
  let end = '';

  //Start of word
  if (Math.random() > 0.5) {
    start = startCons[Math.floor(Math.random()*startCons.length)]
    constLast = true;
  } else {
    start = vowels[Math.floor(Math.random()*vowels.length)]
    constLast = false;
  }

  //Middle of word
  var i;
  for (i = 0; i < (max-2); i++) {
    if (constLast) {
      mid += midVowels[Math.floor(Math.random()*midVowels.length)];
      constLast = false;
    } else {
      mid += midCons[Math.floor(Math.random()*midCons.length)];
      constLast = true;
    }
  }

  //End of word
  if (constLast) {
    end = vowels[Math.floor(Math.random()*vowels.length)]
  } else {
    end = endCons[Math.floor(Math.random()*endCons.length)]
  }

  return start + mid + end;
}
