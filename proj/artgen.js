export default function ( artgen ) {
  let width = 0;
  let height = 0;
  let x1, y1, x2, y2, x3, y3, x4, y4;
  let c1, c2, c3, c4, colorArray;
  let rects, ellipses;
  let maxRects = 20;
  let maxEllipses = 4;
  let i, n;
  let col;

  artgen.setup = function() {
    width = document.getElementById('artgen').clientWidth;
    height = document.getElementById('artgen').clientHeight;
    artgen.createCanvas(width, height);
    artgen.background(255);

    artgen.stroke(0);
    x1 = width/2-width/2.5
    y1 = height/2-height/2.5
    x2 = width/2+width/2.5
    y2 = height/2+height/2.5

    x3 = x1-10
    y3 = y1-10
    x4 = x2+10
    y4 = y2+10

  }

  artgen.draw = function() {
    artgen.stroke(255);
    artgen.fill(255);
    artgen.rect(0,0,width,y1);
    artgen.rect(0,y2,width,height-y2);
    artgen.rect(0,0,x1,height);
    artgen.rect(x2,0,width-x2,height);

    artgen.stroke(1);
    artgen.noFill();
    artgen.rect(x3, y3, x4-x3, y4-y3);
    artgen.rect(x1, y1, x2-x1, y2-y1);
  }

  artgen.gen = function() {
    artgen.stroke(1);
    artgen.fill(255);
    artgen.rect(x1, y1, x2-x1, y2-y1);
    artgen.pickColors();
    if (artgen.random(100) > 50) {
      artgen.drawEllipses();
      artgen.drawRects();
    } else {
      artgen.drawRects();
      artgen.drawEllipses();
    }
  }

  artgen.pickColors = function() {
    c1 = [artgen.random(255),artgen.random(255),artgen.random(255)];
    c2 = [artgen.random(255),artgen.random(255),artgen.random(255)];
    c3 = [artgen.random(255),artgen.random(255),artgen.random(255)];
    c4 = [artgen.random(255),artgen.random(255),artgen.random(255)];
    colorArray = [c1, c2, c3, c4];
    console.log(c1, c2, c3, c4);
  }

  artgen.drawRects = function() {
    rects = 0;
    for (i = 0; i < maxRects; i++) {
      if (artgen.random(100) > 25) {
      rects += 1;
      }
    }

    console.log(rects);

    for (i = 0; i < rects; i++) {
      col = colorArray[Math.floor(artgen.random(4))];

      artgen.stroke(col[0],col[1],col[2]);
      artgen.fill(col[0],col[1],col[2]);
      artgen.quad(
        x1+artgen.random(x2-x1), y1+artgen.random(y2-y1),
        x2-artgen.random(x2-x1), y2-artgen.random(y2-y1),
        x1+artgen.random(x2-x1), y2-artgen.random(y2-y1),
        x2-artgen.random(x2-x1), y1+artgen.random(y2-y1)
      );
    }

  }

  artgen.drawEllipses = function() {
    ellipses = 0;
    for (i = 0; i < maxEllipses; i++) {
      if (artgen.random(100) > 30) {
      ellipses += 1;
      }
    }

    console.log(ellipses);

    for (i = 0; i < ellipses; i++) {
      col = colorArray[Math.floor(artgen.random(4))];

      artgen.stroke(col[0],col[1],col[2]);
      artgen.fill(col[0],col[1],col[2]);
      artgen.ellipse(x1+artgen.random(x2-x1), y1+artgen.random(y2-y1), artgen.random(x2-x1), artgen.random(y2-y1))
    }
  }

}
