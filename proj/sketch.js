export default function ( sketch ) {
  let width = 0;
  let height = 0;
  let a =  50;
  let b =  50;
  let autoActive =  false;
  let sclamp =  false;
  let connectedMode =  true;
  let multishade =  false;
  let currentColor =  [255];
  let rainbow =  true;
  let hidden = false;
  sketch.setup = function() {
  	width = document.getElementById('sketch').clientWidth;
    height = document.getElementById('sketch').clientHeight;
    sketch.createCanvas(width, height);
    sketch.background(255);
    /*sketch.createBackground();*/
  }
  sketch.clr = function() {
    sclamp = false;
    width = document.getElementById('sketch').clientWidth;
    height = document.getElementById('sketch').clientHeight;
    sketch.background(255);
  }
  sketch.sv = function() {
    sketch.save('save.jpg')
  }
  sketch.upd = function(othautoActive, othsclamp, othconnectedMode, othmultishade, othrainbow) {
    autoActive = othautoActive;
    sclamp = othsclamp;
    connectedMode = othconnectedMode;
    multishade = othmultishade;
    rainbow = othrainbow;
  }
  sketch.draw = function() {
  	if (autoActive === true) {
          sketch.auto();
    }
    if (sclamp === true) {
        sketch.stroke(255,0,0)
        sketch.fill(255,0,0);
        sketch.textSize(sketch.random(100));
        sketch.text("sclamp", sketch.random(width), sketch.random(height));
     }

  }
  sketch.createBackground = function () {
     sketch.background(255);
     sketch.stroke(230);
     for (let i = 0; i < 14; i++) {
          sketch.line(i*width/14, 0, i*width/14, height);
          sketch.line(0, i*height/14, width, i*height/14);
     }
  }
  sketch.hide = function () {
    if (hidden == false) {
      $('#help').fadeOut();
      hidden = true;
    } else {
      $('#help').fadeIn();
      hidden = false;
    }
  }
  sketch.auto = function () {
    sketch.line(a, b, sketch.random(width), sketch.random(height));
  }
  sketch.mousePressed = function () {
      if (multishade == false && rainbow == false) {
        currentColor = [sketch.random(230)]
        sketch.stroke(currentColor[0])
      } else if (multishade == false && rainbow == true) {
        currentColor = [sketch.random(255),sketch.random(255),sketch.random(255)]
        sketch.stroke(currentColor[0],currentColor[1],currentColor[2])
      }
      sketch.fill(255)
      a = sketch.mouseX;
      b = sketch.mouseY;
   }
   sketch.mouseDragged = function() {
        if (multishade === true && rainbow === false) {
          currentColor = [sketch.random(230)]
          sketch.stroke(currentColor[0])
        } else if (multishade == true && rainbow === true) {
          currentColor = [sketch.random(255),sketch.random(255),sketch.random(255)]
          sketch.stroke(currentColor[0],currentColor[1],currentColor[2])
        }
        if (autoActive === false && connectedMode === false) {
            sketch.line(a, b, sketch.mouseX, sketch.mouseY);
        } else if (autoActive === false && connectedMode === true){
            sketch.triangle(a, b, sketch.pmouseX, sketch.pmouseY, sketch.mouseX, sketch.mouseY);
        }
    }
}
